## Documentation ##
Présentation de l'application et des fonctions implantées
### Présentation ###
- Auteur : Elouan PETEREAU
- Nom du projet : MyMap
- Date de creation : 16/08/2018
- Lien Gitlab : <https://gitlab.com/elouan/MyMap.git>

### But du projet ###
Créer une application plus complexe sous Android pour apprendre à utiliser les fragments et l'API Goole maps.

L'application permettra de visualiser une liste de points d'intérêt et soit d'en voir le détail puis de s'y rendre via la map soit de directement s'y rendre.

### Détail des fonctionnalitées apportées au projet ###
- Affichage de la liste des points d'intérêts dans une CardView
- Possibilité d'ouvrir le détail de chaque point via un bouton
- Affichage adaptatif de ces deux actions selon le device utilisée (tablette et portable) en utilisant des fragments
- Possibilité d'ouvrir un point d'intérêt sur une carte pour en connaître la position
- Affichage de tous les points de la liste sur une carte
- Trie des points d'intérêts par nom et distance
- Recherche d'un point d'intérêt via son nom
- Utilisation d'une base de donnée pour sauvegarder les point d'intérêts en interne

*D'autres fonctionnalitées apparaîtrons au fur et à mesure de l'avancée du projet.*