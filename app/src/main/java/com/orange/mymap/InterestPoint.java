package com.orange.mymap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

public class InterestPoint implements Parcelable {
    private int id;
    private String title;
    private String description;
    private byte[] picture;
    private double latitude;
    private double longitude;

    public InterestPoint() {
        this.id = -1;
        this.title = null;
        this.description = null;
        this.picture = new byte[0];
        this.latitude = 0;
        this.longitude = 0;
    }

    public InterestPoint(int id, String title, String description, byte[] picture, double latitude, double longitude) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.picture = picture;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private InterestPoint(Parcel in) {
        id = in.readInt();
        title = in.readString();
        description = in.readString();
        picture = new byte[in.readInt()];
        in.readByteArray(picture);
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Bitmap getPicture() {
        return BitmapFactory.decodeByteArray(picture, 0, picture.length);
    }

    public void setPicture(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        this.picture = stream.toByteArray();
    }

    public void setPictureByteArray(byte[] picture) {
        this.picture = picture;
    }

    public byte[] getPictureByteArray() {
        return picture;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "InterestPoint{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(title);
        out.writeString(description);
        out.writeInt(picture.length);
        out.writeByteArray(picture);
        out.writeDouble(latitude);
        out.writeDouble(longitude);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<InterestPoint> CREATOR = new Parcelable.Creator<InterestPoint>() {
        public InterestPoint createFromParcel(Parcel in) {
            return new InterestPoint(in);
        }

        public InterestPoint[] newArray(int size) {
            return new InterestPoint[size];
        }
    };


}
