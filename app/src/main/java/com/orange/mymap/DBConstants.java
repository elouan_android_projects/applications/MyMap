package com.orange.mymap;

public interface DBConstants {
    String TABLE_INTERESTPOINTLIST = "TABLE_INTERESTPOINT";
    String COL_ID = "ID";
    String COL_TITLE = "TITLE";
    String COL_DESCRIPTION = "DESCRIPTION";
    String COL_PICTURE = "PICTURE";
    String COL_LATITUDE = "LATITUDE";
    String COL_LONGITUDE = "LONGITUDE";
    int DB_VERSION = 1;
    String DB_NAME = "InterestPointList.db";
    int NUM_COL_ID = 0;
    int NUM_COL_TITLE = 1;
    int NUM_COL_DESCRIPTION = 2;
    int NUM_COL_PICTURE = 3;
    int NUM_COL_LATITUDE = 4;
    int NUM_COL_LONGITUDE = 5;
}
