package com.orange.mymap.fragments;


import android.content.Context;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;
import com.orange.mymap.InterestPoint;
import com.orange.mymap.InterestPointAdapter;
import com.orange.mymap.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class InterestPointListFragment extends Fragment implements InterestPointAdapter.RecyclerViewClickListener {

    private final String TAG = getClass().getSimpleName();

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private ArrayList<InterestPoint> interestPointList;

    public InteresPointListButtonListener listener;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_interest_point_list, container, false);

        recyclerView = view.findViewById(R.id.recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm);

        // improve performance (only if changes in content do not change the layout size of the RecyclerView)
        recyclerView.setHasFixedSize(true);

        // specify an adapter
        updateAdapter(interestPointList);

        return view;
    }

    public void updateAdapter(ArrayList<InterestPoint> list) {
        adapter = new InterestPointAdapter(getActivity(), list, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        interestPointList = new ArrayList<>();
    }

    public void setInterestPointList(ArrayList<InterestPoint> interestPointList) {
        this.interestPointList = interestPointList;
        updateAdapter(interestPointList);
    }

    public ArrayList<InterestPoint> getInterestPointList() {
        return interestPointList;
    }

    public interface InteresPointListButtonListener {
        int ACTION_DETAILS = 1;
        int ACTION_SHOW = 2;

        void InterestPointListButtonClicked(InterestPoint interestPoint, int action);
    }

    @Override
    public void recyclerViewButtonClicked(View v, int position, int action) {
        switch (action) {
            case ACTION_SHOW:
                Log.d(TAG, "Show = " + interestPointList.get(position).getTitle());
                listener.InterestPointListButtonClicked(interestPointList.get(position), InteresPointListButtonListener.ACTION_SHOW);
                break;
            case ACTION_DETAILS:
                Log.d(TAG, "Details = " + interestPointList.get(position).getTitle());
                listener.InterestPointListButtonClicked(interestPointList.get(position), InteresPointListButtonListener.ACTION_DETAILS);
                break;
            default:
                break;
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (InteresPointListButtonListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement InteresPointListButtonListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public void sortListByName() {
        Collections.sort(interestPointList, new Comparator<InterestPoint>() {
            @Override
            public int compare(InterestPoint interestPoint1, InterestPoint interestPoint2) {
                return interestPoint1.getTitle().compareTo(interestPoint2.getTitle());
            }
        });
        updateAdapter(interestPointList);
    }

    public void sortListByPosition(final LatLng latLng) {
        Collections.sort(interestPointList, new Comparator<InterestPoint>() {
            @Override
            public int compare(InterestPoint i1, InterestPoint i2) {
                double distI1 = Math.sqrt(Math.exp(i1.getLongitude() - latLng.longitude) + Math.exp(i1.getLatitude() - latLng.latitude));
                double distI2 = Math.sqrt(Math.exp(i2.getLongitude() - latLng.longitude) + Math.exp(i2.getLatitude() - latLng.latitude));
                if (distI1 < distI2) {
                    return 1;
                } else if (distI1 == distI2) {
                    return 0;
                } else {
                    return -1;
                }
            }
        });
        updateAdapter(interestPointList);

    }

    public void searchInterestPoint(String s) {
        ArrayList<InterestPoint> searchList = new ArrayList<>();

        for (InterestPoint p : interestPointList) {
            if (p.getTitle().toLowerCase().contains(s.toLowerCase())) {
                searchList.add(p);
            }
        }

        updateAdapter(searchList);
    }

}