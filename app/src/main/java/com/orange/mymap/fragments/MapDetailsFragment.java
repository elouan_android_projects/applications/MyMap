package com.orange.mymap.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import static com.orange.mymap.activity.MapActivity.MARKERINTERESTPOINT_KEY;

import com.orange.mymap.InterestPoint;
import com.orange.mymap.R;

public class MapDetailsFragment extends Fragment {

    private TextView title;
    private TextView description;
    private ImageView imagePreview;
    private Button directionButton;
    private Button detailsButton;

    private InterestPoint interestPoint;

    private final String TAG = getClass().getSimpleName();
    public MapDetailsButtonListener clickListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        Log.d(TAG, "bundle attached to details fragment = " + bundle);
        if (bundle == null) {
            interestPoint = new InterestPoint();
        } else {
            interestPoint = getArguments().getParcelable(MARKERINTERESTPOINT_KEY);
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mapdetails, container, false);
        title = view.findViewById(R.id.mapdetails_title);
        description = view.findViewById(R.id.mapdetails_description);
        imagePreview = view.findViewById(R.id.mapdetails_image_preview);


        // Set Content
        Log.d(TAG, "interest point = " + interestPoint);
        title.setText(interestPoint.getTitle());
        description.setText(interestPoint.getDescription());
        imagePreview.setImageBitmap(interestPoint.getPicture());

        // Set Buttons listener
        directionButton = view.findViewById(R.id.mapdetails_button_direction);
        directionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "interestPoint = " + interestPoint);
                clickListener.DetailsButtonClicked(interestPoint, MapDetailsButtonListener.ACTION_DIRECTION);
            }
        });
        detailsButton = view.findViewById(R.id.mapdetails_button_details);
        detailsButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    detailsButton.setBackgroundColor(Color.parseColor("#22000000"));
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    detailsButton.setBackgroundColor(Color.parseColor("#00000000"));
                    clickListener.DetailsButtonClicked(interestPoint, MapDetailsButtonListener.ACTION_DETAILS);
                } else if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                    detailsButton.setBackgroundColor(Color.parseColor("#00000000"));
                }
                return false;
            }
        });

        return view;
    }

    public interface MapDetailsButtonListener {
        int ACTION_DIRECTION = 1;
        int ACTION_DETAILS = 2;

        void DetailsButtonClicked(InterestPoint interestPoint, int action);
    }

    public static MapDetailsFragment newInstance(InterestPoint interestPoint) {
        MapDetailsFragment f = new MapDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(MARKERINTERESTPOINT_KEY, interestPoint);
        f.setArguments(bundle);
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            clickListener = (MapDetailsFragment.MapDetailsButtonListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement MapDetailsButtonListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        clickListener = null;
    }

}
