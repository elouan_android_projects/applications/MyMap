package com.orange.mymap.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.orange.mymap.InterestPoint;
import com.orange.mymap.R;
import com.orange.mymap.activity.MainActivity;
import com.orange.mymap.activity.MapActivity;

import static com.orange.mymap.activity.MainActivity.INTERESTPOINT_KEY;


public class DetailsFragment extends Fragment {

    private TextView title;
    private TextView description;
    private ImageView imagePreview;
    private Button showInMap;

    private InterestPoint interestPoint;

    private final String TAG = getClass().getSimpleName();
    public DetailsButtonListener clickListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        Log.d(TAG, "bundle attached to details fragment = " + bundle);
        if (bundle == null) {
            interestPoint = new InterestPoint();
        } else {
            interestPoint = getArguments().getParcelable(INTERESTPOINT_KEY);
        }
    }


    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);

        title = view.findViewById(R.id.details_title);
        description = view.findViewById(R.id.details_description);
        imagePreview = view.findViewById(R.id.details_image_preview);
        showInMap = view.findViewById(R.id.details_button_show);


        // Set Content
        title.setText(interestPoint.getTitle());
        description.setText(interestPoint.getDescription());
        imagePreview.setImageBitmap(interestPoint.getPicture());

        if (!getActivity().getClass().getName().equals(MainActivity.class.getName())) {
            if (getActivity().getCallingActivity().getClassName().equals(MapActivity.class.getName())) {
                showInMap.setVisibility(View.GONE);
            } else {
                showInMap.setVisibility(View.VISIBLE);
            }
        } else if (interestPoint.getId() == -1) {
            showInMap.setVisibility(View.GONE);
        }

        // Set Onclick listener
        showInMap = view.findViewById(R.id.details_button_show);
        showInMap.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "interestPoint = " + interestPoint);
                clickListener.DetailsButtonClicked(interestPoint, DetailsButtonListener.ACTION_SHOW);
            }
        });


        return view;
    }

    public interface DetailsButtonListener {
        int ACTION_SHOW = 2;

        void DetailsButtonClicked(InterestPoint interestPoint, int action);
    }

    public static DetailsFragment newInstance(InterestPoint interestPoint) {
        DetailsFragment f = new DetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(INTERESTPOINT_KEY, interestPoint);
        f.setArguments(bundle);
        return f;
    }

    public InterestPoint getParcelableInterestPoint() {
        return (InterestPoint) getArguments().getParcelable(INTERESTPOINT_KEY);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            clickListener = (DetailsFragment.DetailsButtonListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement DetailsButtonListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        clickListener = null;
    }

}
