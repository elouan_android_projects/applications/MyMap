package com.orange.mymap;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class InterestPointAdapter extends RecyclerView.Adapter<InterestPointAdapter.InterrestPointViewHolder> {

    private Context mContext;
    static RecyclerViewClickListener clickListener;
    private List<InterestPoint> interestPointsList;

    // Provide a reference to the views for each data item
    public static class InterrestPointViewHolder extends RecyclerView.ViewHolder implements View.OnTouchListener {
        private TextView title, description;
        private ImageView imagePreview;
        private Button detailsButton, showButton;


        @SuppressLint("ClickableViewAccessibility")
        InterrestPointViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.interest_point_title);
            description = (TextView) view.findViewById(R.id.interest_point_description);
            imagePreview = (ImageView) view.findViewById(R.id.interest_point_image_preview);

            detailsButton = view.findViewById(R.id.interest_point_button_details);
            detailsButton.setOnTouchListener(this);
            showButton = view.findViewById(R.id.interest_point_button_show);
            showButton.setOnTouchListener(this);
        }

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (view.getId() == detailsButton.getId()) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    detailsButton.setBackgroundColor(Color.parseColor("#22000000"));
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    // send data to activity
                    detailsButton.setBackgroundColor(Color.parseColor("#00000000"));
                    clickListener.recyclerViewButtonClicked(view, this.getLayoutPosition(), RecyclerViewClickListener.ACTION_DETAILS);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                    detailsButton.setBackgroundColor(Color.parseColor("#00000000"));
                }
            } else if (view.getId() == showButton.getId()) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    showButton.setBackgroundColor(Color.parseColor("#22000000"));
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    // send data to activity
                    showButton.setBackgroundColor(Color.parseColor("#00000000"));
                    clickListener.recyclerViewButtonClicked(view, this.getLayoutPosition(), RecyclerViewClickListener.ACTION_SHOW);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                    showButton.setBackgroundColor(Color.parseColor("#00000000"));
                }
            }
            return false;
        }
    }

    // Interface to send info to fragment
    public interface RecyclerViewClickListener {
        int ACTION_DETAILS = 1;
        int ACTION_SHOW = 2;

        void recyclerViewButtonClicked(View v, int position, int action);
    }

    // Provide a suitable constructor
    public InterestPointAdapter(Context mContext, List<InterestPoint> interestPointsList, RecyclerViewClickListener clickListener) {
        this.mContext = mContext;
        this.interestPointsList = interestPointsList;
        InterestPointAdapter.clickListener = clickListener;
    }


    // Create new views
    @NonNull
    @Override
    public InterrestPointViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_interest_point, parent, false);
        return new InterrestPointViewHolder(v);
    }

    // Replace the contents of a view
    @Override
    public void onBindViewHolder(@NonNull InterrestPointViewHolder holder, int position) {
        InterestPoint interestPoint = interestPointsList.get(position);
        holder.title.setText(interestPoint.getTitle());
        holder.description.setText(interestPoint.getDescription());
        holder.imagePreview.setImageBitmap(interestPoint.getPicture());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Return the size of your dataset
    @Override
    public int getItemCount() {
        return interestPointsList.size();
    }


}
