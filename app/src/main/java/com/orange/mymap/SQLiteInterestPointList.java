package com.orange.mymap;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SQLiteInterestPointList extends SQLiteOpenHelper implements DBConstants {

    private final String TAG = getClass().getSimpleName();

    private static final String CREATE_DB = "CREATE TABLE " + TABLE_INTERESTPOINTLIST + "("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COL_TITLE + " TEXT NOT NULL, "
            + COL_DESCRIPTION + " TEXT NOT NULL, "
            + COL_PICTURE + " BLOB NOT NULL, "
            + COL_LATITUDE + " REAL NOT NULL, "
            + COL_LONGITUDE + " REAL NOT NULL "
            + ");";

    private Context mContext;
    public String mDatabaseName;

    public SQLiteInterestPointList(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        Log.d(TAG, "database path : " + context.getDatabasePath(name));
        mContext = context;
        mDatabaseName = name;
        File file = mContext.getDatabasePath(mDatabaseName);
        try {
            copyDB(file);
        }catch (IOException e){
            Log.e(TAG,"Error : copy db failed "+e);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(TAG, "onCreate");
        sqLiteDatabase.execSQL(CREATE_DB);

    }

    private void copyDB(File dst) throws IOException {
        try (InputStream in = mContext.getResources().openRawResource(R.raw.interestpoint)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }


    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        Log.d(TAG, "onOpen");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE " + TABLE_INTERESTPOINTLIST + ";");
        onCreate(sqLiteDatabase);
    }
}
