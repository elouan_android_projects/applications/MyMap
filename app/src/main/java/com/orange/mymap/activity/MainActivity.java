package com.orange.mymap.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.orange.mymap.InterestPoint;
import com.orange.mymap.fragments.DetailsFragment;
import com.orange.mymap.fragments.InterestPointListFragment;
import com.orange.mymap.InterestPointListDB;
import com.orange.mymap.R;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements InterestPointListFragment.InteresPointListButtonListener, DetailsFragment.DetailsButtonListener {

    private final String TAG = getClass().getSimpleName();
    public static final String INTERESTPOINT_KEY = "interestPoint_key";
    public static final String INTERESTPOINTLIST_KEY = "interestPointList_key";
    private final int SHOW_DETAILS = 101;

    private boolean dualPane;
    private InterestPoint interestPointSelected;
    private ArrayList<InterestPoint> interestPointSelectedList;
    private InterestPointListDB interestPointListDB;

    private InterestPointListFragment interestPointListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Set activity view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Check permissions
        checkPermissions();

        // Set database
        interestPointListDB = new InterestPointListDB(this);

        interestPointListDB.open();
        Log.d(TAG, "DB content : " + interestPointListDB.getListFromDatabase());
        interestPointListDB.close();


        try {
            FragmentManager fragmentManager = this.getSupportFragmentManager();
            interestPointListFragment = (InterestPointListFragment) fragmentManager.findFragmentById(R.id.list_fragment);
            interestPointListDB.open();
            interestPointListFragment.setInterestPointList(interestPointListDB.getListFromDatabase());
            interestPointListFragment.sortListByName();
            interestPointListDB.close();
            interestPointSelectedList = interestPointListFragment.getInterestPointList();
            Log.d(TAG, "Selected list content : " + interestPointSelectedList);
        } catch (NullPointerException e) {
            Log.e(TAG, "Error while retrieving the list of interest point " + e);
            interestPointSelectedList = new ArrayList<>();
        }

        // Set the toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set the details fragment
        View detailsFrame = findViewById(R.id.details_fragment);
        dualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;
        if (interestPointSelected == null) {
            interestPointSelected = new InterestPoint();
        }
    }

    private void checkPermissions() {
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
            Log.d(TAG, "Request permission");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dualPane) {
            Log.d(TAG, "dual pane interest point selected = " + interestPointSelected);
            showDetails(interestPointSelected);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelable(INTERESTPOINT_KEY, interestPointSelected);
        Log.d(TAG, "interestPoint Saved = " + savedInstanceState.getParcelable(INTERESTPOINT_KEY));
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        interestPointSelected = savedInstanceState.getParcelable(INTERESTPOINT_KEY);
        Log.d(TAG, "interestPoint Loaded = " + savedInstanceState.getParcelable(INTERESTPOINT_KEY));
    }

    @Override
    public void InterestPointListButtonClicked(InterestPoint interestPoint, int action) {
        Log.d(TAG, "button clicked : " + action + "\nInterest point selected : " + interestPoint);
        switch (action) {
            case InterestPointListFragment.InteresPointListButtonListener.ACTION_SHOW:
                interestPointSelected = interestPoint;
                showPoint(interestPoint);
                Log.d(TAG, "show point : " + interestPoint);
                break;
            case InterestPointListFragment.InteresPointListButtonListener.ACTION_DETAILS:
                interestPointSelected = interestPoint;
                showDetails(interestPoint);
                Log.d(TAG, "show details of : " + interestPoint);
                break;
            default:
                break;
        }
    }

    @Override
    public void DetailsButtonClicked(InterestPoint interestPoint, int action) {
        Log.d(TAG, "button clicked : " + action + "\nInterest point selected : " + interestPoint);
        switch (action) {
            case DetailsFragment.DetailsButtonListener.ACTION_SHOW:
                interestPointSelected = interestPoint;
                showPoint(interestPoint);
                Log.d(TAG, "show point : " + interestPoint);
                break;
            default:
                break;
        }
    }

    public void showPoint(InterestPoint interestPoint) {
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra(INTERESTPOINT_KEY, interestPoint);
        startActivity(intent);
    }

    public void showDetails(InterestPoint interestPoint) {
        if (dualPane) {

            DetailsFragment details = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.details_fragment);
            if (details == null || details.getParcelableInterestPoint() != interestPoint) {
                // Make new fragment to show this selection.
                details = DetailsFragment.newInstance(interestPoint);

                // Execute a transaction, replacing any existing fragment with this one inside the frame.
                getSupportFragmentManager().beginTransaction().replace(R.id.details_fragment, details).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
            }
        } else {
            // Otherwise we need to launch a new activity to display
            // the dialog fragment with selected text.
            Intent intent = new Intent();
            intent.setClass(this, DetailsActivity.class);
            intent.putExtra(INTERESTPOINT_KEY, interestPoint);
            startActivityForResult(intent, SHOW_DETAILS);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                interestPointSelected = data.getParcelableExtra(INTERESTPOINT_KEY);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu
        getMenuInflater().inflate(R.menu.main_menu, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.d(TAG, "Search interst point containing : " + s);
                interestPointListFragment.searchInterestPoint(s);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_map:
                Intent intent = new Intent(this, MapActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(INTERESTPOINTLIST_KEY, interestPointSelectedList);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.action_sort_by_name:
                interestPointListFragment.sortListByName();
                break;
            case R.id.action_sort_by_distance:
                LatLng position = getLocation();
                if (position == new LatLng(0, 0)) {
                    Toast.makeText(this, R.string.toast_locate_error, Toast.LENGTH_SHORT).show();
                } else {
                    interestPointListFragment.sortListByPosition(position);
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public LatLng getLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            Log.d(TAG, "Request permission");
        }

        // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();
        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);
        // Getting Current Location
        Location location = locationManager.getLastKnownLocation(provider);
        LatLng myPosition;

        if (location != null) {
            // Getting latitude & longitude of the current location
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            // Creating a LatLng object for the current location and center the camera on position
            myPosition = new LatLng(latitude, longitude);

        } else {
            myPosition = new LatLng(0, 0);
        }

        return myPosition;

    }


}
