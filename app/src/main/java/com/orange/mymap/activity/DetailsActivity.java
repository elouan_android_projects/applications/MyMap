package com.orange.mymap.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.orange.mymap.InterestPoint;
import com.orange.mymap.R;
import com.orange.mymap.fragments.DetailsFragment;

import java.util.Objects;

import static com.orange.mymap.activity.MainActivity.INTERESTPOINT_KEY;

public class DetailsActivity extends AppCompatActivity implements DetailsFragment.DetailsButtonListener {

    private final String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        Intent intent = new Intent();
        Log.d(TAG, "Extra content returned = " + getIntent().getExtras().getParcelable(INTERESTPOINT_KEY));
        intent.putExtra(INTERESTPOINT_KEY, getIntent().getExtras().getParcelable(INTERESTPOINT_KEY));

        setResult(RESULT_OK, intent);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (!Objects.requireNonNull(getCallingActivity()).getClassName().equals(MapActivity.class.getName())) {
                // Screen in landscape mode -> activity closed
                Log.d(TAG, "activity Details finished");
                finish();
                return;
            }
        }

        // Give Arguments to fragment
        DetailsFragment details = new DetailsFragment();
        details.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction().replace(R.id.details_frameLayout, details).commit();
        Log.d(TAG, "Extra content sended = " + getIntent().getExtras());


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                Log.d(TAG, "activity Details finished");
                break;
        }
        return true;
    }


    @Override
    public void DetailsButtonClicked(InterestPoint interestPoint, int action) {
        Log.d(TAG, "button clicked : " + action + "\nInterest point selected : " + interestPoint);
        switch (action) {
            case ACTION_SHOW:
                finish();
                Intent intent = new Intent();
                intent.setClass(this, MapActivity.class);
                intent.putExtra(INTERESTPOINT_KEY, interestPoint);
                startActivity(intent);
                Log.d(TAG, "show point : " + interestPoint);
                break;
            default:
                break;
        }

    }

}
