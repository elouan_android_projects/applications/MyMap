package com.orange.mymap.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.EncodedPolyline;
import com.google.maps.model.TravelMode;
import com.orange.mymap.InterestPoint;
import com.orange.mymap.R;
import com.orange.mymap.fragments.MapDetailsFragment;

import java.util.ArrayList;
import java.util.List;

import static com.orange.mymap.activity.MainActivity.INTERESTPOINTLIST_KEY;
import static com.orange.mymap.activity.MainActivity.INTERESTPOINT_KEY;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, MapDetailsFragment.MapDetailsButtonListener {

    public static final String MARKERINTERESTPOINT_KEY = "markerinterestPoint_key";

    private final String TAG = getClass().getSimpleName();
    private final int SHOW_SIMPLEDETAILS = 111;

    private final int INTERESTPOINT_EMPTY = 0;
    private final int INTERESTPOINT_SINGLE = 1;
    private final int INTERESTPOINT_LIST = 2;

    private GoogleMap mMap;
    private int extraDataType;
    private InterestPoint interestPoint;
    private ArrayList<InterestPoint> interestPointList;
    private Polyline polylineSelected;

    private FloatingActionButton getLocationButton;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // Set Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        frameLayout = findViewById(R.id.mapDetails_frameLayout);

        //Set Buttons
        getLocationButton = findViewById(R.id.locateActionButton);

        // Get extra content
        Bundle extra = getIntent().getExtras();
        if (extra.containsKey(INTERESTPOINT_KEY)) {
            interestPoint = extra.getParcelable(INTERESTPOINT_KEY);
            extraDataType = INTERESTPOINT_SINGLE;
            Log.d(TAG, "one interest point sended");

        } else if (extra.containsKey(INTERESTPOINTLIST_KEY)) {
            interestPointList = extra.getParcelableArrayList(INTERESTPOINTLIST_KEY);
            extraDataType = INTERESTPOINT_LIST;
            Log.d(TAG, "interest point list sended");
        } else {
            extraDataType = INTERESTPOINT_EMPTY;
            Log.d(TAG, "nothing sended");
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void setInterestPointMap() {
        Log.d(TAG, "create a marker for the interest point : " + interestPoint);
        // Add a marker to the interest point position and move the camera
        LatLng point = new LatLng(interestPoint.getLatitude(), interestPoint.getLongitude());
        mMap.addMarker(new MarkerOptions().position(point).title(interestPoint.getTitle()));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(point));
    }

    public void setInterestPointListMap() {
        Log.d(TAG, "create markers for the list interest points : " + interestPointList);
        // Add a marker to the interest point position and move the camera
        LatLngBounds.Builder interestPointLatLngBounds = new LatLngBounds.Builder();
        for (InterestPoint p : interestPointList) {
            interestPointLatLngBounds.include(new LatLng(p.getLatitude(), p.getLongitude())).build();
        }
        if (interestPointList.size() != 0) {
            LatLngBounds centerPointPosition = interestPointLatLngBounds.build();
            Log.d(TAG, "centerPointPosition = " + centerPointPosition.getCenter());
            for (InterestPoint p : interestPointList) {
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(p.getLatitude(), p.getLongitude()))
                        .title(p.getTitle()));
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLng(centerPointPosition.getCenter()));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);

        switch (extraDataType) {
            case INTERESTPOINT_EMPTY:
                break;
            case INTERESTPOINT_SINGLE:
                // Set single marker
                setInterestPointMap();
                break;
            case INTERESTPOINT_LIST:
                // Set list of markers
                setInterestPointListMap();
        }

        // Set map bounds
        LatLngBounds mapLimitSingle = new LatLngBounds(new LatLng(48.754012, -3.460415), new LatLng(48.757251, -3.451821));
        mMap.setLatLngBoundsForCameraTarget(mapLimitSingle);

        mMap.setMaxZoomPreference(20);
        mMap.setMinZoomPreference(16);


        // Set map listener
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            public void onMapClick(LatLng latLng) {
                Log.d(TAG, "test");
                frameLayout.setVisibility(View.GONE);
            }
        });

        getLocationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LatLng position = getLocation();
                mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
                Toast.makeText(MapActivity.this, R.string.toast_locate, Toast.LENGTH_SHORT).show();
            }
        });

        setMarkerListener();
        setShape();


    }

    private void setMarkerListener() {
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.hideInfoWindow();
                Log.d(TAG, "extradat : " + extraDataType);
                switch (extraDataType) {
                    case INTERESTPOINT_EMPTY:
                        break;
                    case INTERESTPOINT_SINGLE:
                        if (marker.getTitle().equals(interestPoint.getTitle())) {
                            showInterestPointDetails(interestPoint);
                        }
                        break;
                    case INTERESTPOINT_LIST:
                        int i = 0;
                        boolean found = false;
                        while ((i < interestPointList.size() && !found)) {
                            if (marker.getTitle().equals(interestPointList.get(i).getTitle())) {
                                found = true;
                            } else {
                                i++;
                            }
                        }
                        showInterestPointDetails(interestPointList.get(i));
                        break;
                }
                return true;
            }
        });
    }

    private void setShape() {
        Polygon polygon = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(48.756822, -3.454097), new LatLng(48.757056, -3.453904), new LatLng(48.757053, -3.453896), new LatLng(48.757092, -3.453864), new LatLng(48.757103, -3.453873), new LatLng(48.757476, -3.453563), new LatLng(48.757425, -3.453417), new LatLng(48.757047, -3.453724), new LatLng(48.757051, -3.453738), new LatLng(48.757007, -3.453776), new LatLng(48.757004, -3.453765), new LatLng(48.756774, -3.453959))
                .fillColor(Color.RED)
                .strokeColor(Color.TRANSPARENT));
        polygon.setTag("Batiment Soft");
        polygon.setClickable(true);
        setShapeListener(polygon);
    }

    private void setShapeListener(Polygon polygon) {
        mMap.setOnPolygonClickListener(new GoogleMap.OnPolygonClickListener() {
            public void onPolygonClick(Polygon polygon) {
                Toast toast = Toast.makeText(MapActivity.this, (String) polygon.getTag(), Toast.LENGTH_SHORT);
                Toolbar too = findViewById(R.id.toolbar);
                toast.setGravity(Gravity.TOP, 0, (int) too.getHeight() + 40);
                toast.show();
            }
        });
    }

    public void showInterestPointDetails(InterestPoint p) {
        Log.d(TAG, "show details of interestpoint : " + p);

        MapDetailsFragment details = MapDetailsFragment.newInstance(p);
        frameLayout.setVisibility(View.VISIBLE);
        getSupportFragmentManager().beginTransaction().replace(R.id.mapDetails_frameLayout, details).commit();

        Log.d(TAG, "Extra content sended = " + getIntent().getExtras());
    }

    private LatLng getLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            Log.d(TAG, "Request permission");
        }

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

        // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();
        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);
        // Getting Current Location
        Location location = locationManager.getLastKnownLocation(provider);
        LatLng position;
        if (location != null) {
            // Getting latitude & longitude of the current location
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            // Creating a LatLng object for the current location and center the camera on position
            position = new LatLng(latitude, longitude);

        } else {
            position = new LatLng(0, 0);
        }
        return position;
    }

    @Override
    public void DetailsButtonClicked(InterestPoint interestPoint, int action) {
        Log.d(TAG, "button clicked : " + action + "\nInterest point selected : " + interestPoint);
        switch (action) {
            case MapDetailsFragment.MapDetailsButtonListener.ACTION_DIRECTION:
                Log.d(TAG, "direction to point : " + interestPoint);
                // Getting URL to the Google Directions API
                LatLng origin = new LatLng(interestPoint.getLatitude(), interestPoint.getLongitude());
                LatLng dest = getLocation();

                List<LatLng> path = getDirection(origin, dest);

                // Draw the polyline -> draw a straight line bewtween two point
                // Need to call direction API for real route
                if (polylineSelected != null) {
                    polylineSelected.remove();
                }
                if (path.size() > 0) {
                    PolylineOptions opts = new PolylineOptions().addAll(path).color(Color.BLUE).width(5);
                    polylineSelected = mMap.addPolyline(opts);
                }


                break;
            case MapDetailsFragment.MapDetailsButtonListener.ACTION_DETAILS:
                Log.d(TAG, "open details point : " + interestPoint);
                Intent intent = new Intent(this, DetailsActivity.class);
                intent.putExtra(INTERESTPOINT_KEY, interestPoint);
                startActivityForResult(intent, SHOW_SIMPLEDETAILS);
                break;
            default:
                break;
        }
    }

    private List<LatLng> getDirection(LatLng origin, LatLng destination) {

        //Define list to get all latlng for the route
        List<LatLng> path = new ArrayList();
        //Execute Directions API request
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("AIzaSyC0PoF229IfLysGcoJN9BOnLUswTCTQmsU  ")
                .build();
        DirectionsApiRequest req = DirectionsApi.getDirections(context, origin.latitude + "," + origin.longitude, destination.latitude + "," + destination.longitude);
        req.mode(TravelMode.WALKING);
        try {
            DirectionsResult res = req.await();

            //Loop through legs and steps to get encoded polylines of each step
            if (res.routes != null && res.routes.length > 0) {
                DirectionsRoute route = res.routes[0];

                if (route.legs != null) {
                    for (int i = 0; i < route.legs.length; i++) {
                        DirectionsLeg leg = route.legs[i];
                        if (leg.steps != null) {
                            for (int j = 0; j < leg.steps.length; j++) {
                                DirectionsStep step = leg.steps[j];
                                if (step.steps != null && step.steps.length > 0) {
                                    for (int k = 0; k < step.steps.length; k++) {
                                        DirectionsStep step1 = step.steps[k];
                                        EncodedPolyline points1 = step1.polyline;
                                        if (points1 != null) {
                                            //Decode polyline and add points to list of route coordinates
                                            List<com.google.maps.model.LatLng> coords1 = points1.decodePath();
                                            for (com.google.maps.model.LatLng coord1 : coords1) {
                                                path.add(new LatLng(coord1.lat, coord1.lng));
                                            }
                                        }
                                    }
                                } else {
                                    EncodedPolyline points = step.polyline;
                                    if (points != null) {
                                        //Decode polyline and add points to list of route coordinates
                                        List<com.google.maps.model.LatLng> coords = points.decodePath();
                                        for (com.google.maps.model.LatLng coord : coords) {
                                            path.add(new LatLng(coord.lat, coord.lng));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getLocalizedMessage());
        }
        return path;
    }

}
