package com.orange.mymap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class InterestPointListDB implements DBConstants {

    private SQLiteDatabase db;
    private SQLiteInterestPointList dbSQLite;

    public InterestPointListDB(Context context) {
        dbSQLite = new SQLiteInterestPointList(context, DB_NAME, null, DB_VERSION);
    }

    public void open() {
        db = dbSQLite.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    public long insertInterestPoint(InterestPoint interestPoint) {
        // ContentValues work like HashMap
        ContentValues values = new ContentValues();
        values.put(COL_TITLE, interestPoint.getTitle());
        values.put(COL_DESCRIPTION, interestPoint.getDescription());
        values.put(COL_PICTURE, interestPoint.getPictureByteArray());
        values.put(COL_LATITUDE, interestPoint.getLatitude());
        values.put(COL_LONGITUDE, interestPoint.getLongitude());

        return db.insert(TABLE_INTERESTPOINTLIST, null, values);
    }

    public int upDateInterestPoint(int id, InterestPoint interestPoint) {
        ContentValues values = new ContentValues();
        values.put(COL_TITLE, interestPoint.getTitle());
        values.put(COL_DESCRIPTION, interestPoint.getDescription());
        values.put(COL_PICTURE, interestPoint.getPictureByteArray());
        values.put(COL_LATITUDE, interestPoint.getLatitude());
        values.put(COL_LONGITUDE, interestPoint.getLongitude());

        return db.update(TABLE_INTERESTPOINTLIST, values, COL_ID + "=" + id, null);
    }

    public void removeInterestPoint(int id) {
        db.delete(TABLE_INTERESTPOINTLIST, COL_ID + "=" + id, null);
    }

    public ArrayList<InterestPoint> getListFromDatabase() {
        ArrayList<InterestPoint> mailList = new ArrayList<InterestPoint>();
        if (db != null) {
            Cursor c = db.query(TABLE_INTERESTPOINTLIST, new String[]{COL_ID, COL_TITLE, COL_DESCRIPTION, COL_PICTURE, COL_LATITUDE, COL_LONGITUDE}, null, null, null, null, null);
            if (c.moveToFirst()) {
                mailList.add(new InterestPoint(c.getInt(NUM_COL_ID), c.getString(NUM_COL_TITLE), c.getString(NUM_COL_DESCRIPTION), c.getBlob(NUM_COL_PICTURE), c.getDouble(NUM_COL_LATITUDE), c.getDouble(NUM_COL_LONGITUDE)));
                while (c.moveToNext()) {
                    mailList.add(new InterestPoint(c.getInt(NUM_COL_ID), c.getString(NUM_COL_TITLE), c.getString(NUM_COL_DESCRIPTION), c.getBlob(NUM_COL_PICTURE), c.getDouble(NUM_COL_LATITUDE), c.getDouble(NUM_COL_LONGITUDE)));
                }
            }

            c.close();
        }
        return mailList;
    }

}
